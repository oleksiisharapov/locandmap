package com.loactionandmap.loactionandmap

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    val items = mapOf("a" to listOf(1, 2), "b" to listOf(4, 5))

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test(){
        assertEquals(listOf("a", 1, 2, "b", 4, 5), flatter(items))
    }

    fun flatter(inputMap: Map<String, List<Int>>): List<Any>{
        return inputMap.flatMap {( key, value) -> listOf(key) + value }
    }



}
