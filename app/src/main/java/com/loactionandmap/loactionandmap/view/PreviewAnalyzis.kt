package com.loactionandmap.loactionandmap.view

import android.content.Context
import android.graphics.Matrix
import android.hardware.display.DisplayManager
import android.os.Handler
import android.os.HandlerThread
import android.util.Rational
import android.util.Size
import android.view.*
import androidx.camera.core.*
import com.loactionandmap.loactionandmap.ml.FaceAnalyzer
import java.lang.IllegalArgumentException
import java.lang.ref.WeakReference
import java.util.*

class PreviewAnalyzis(
    previewConfig: PreviewConfig,
    analysisConfig: ImageAnalysisConfig,
    viewFinderRef: WeakReference<TextureView>,
    overlayViewRef: WeakReference<FacePointsView>
) {

    val previewUseCase: Preview?
    val analysisUseCase: ImageAnalysis?

    private lateinit var displayManager: DisplayManager

    private var bufferRotation = 0
    private var viewFinderRotation: Int? = null

    private var bufferDimens = Size(0, 0)
    private var viewFinderDimensions = Size(0, 0)
    private var previousAnalysisDimensions = Size(0, 0)
    private var previousViewDimensions = Size(0, 0)


    private val displayListener = object : DisplayManager.DisplayListener {

        override fun onDisplayChanged(p0: Int) {
            val viewFinder = viewFinderRef.get() ?: return
            if (p0 == -1) {
                val display = displayManager.getDisplay(p0)
                val rotation = getDisplaySurfaceRotation(display)
                updateTransform(viewFinder, rotation, bufferDimens, viewFinderDimensions)
                updateOverlayTransform(overlayViewRef.get(), previousViewDimensions)
            }
        }

        override fun onDisplayAdded(p0: Int) {
        }

        override fun onDisplayRemoved(p0: Int) {
        }

    }


    init {

        val viewfinder = viewFinderRef.get() ?: throw IllegalArgumentException("Wrong reference to viewfinder")

        viewFinderRotation = getDisplaySurfaceRotation(viewFinderRef.get()?.display)

        previewUseCase = Preview(previewConfig)
        analysisUseCase = ImageAnalysis(analysisConfig).apply {
            analyzer = FaceAnalyzer().apply {
                pointUpdateListener = { points ->
                    overlayViewRef.get()?.facePoints = points
                }
                analysisStateListener = {
                    updateOverlayTransform(overlayViewRef.get(), it)
                }
            }
        }

        previewUseCase.onPreviewOutputUpdateListener = Preview.OnPreviewOutputUpdateListener {
            val viewFinder = viewFinderRef.get() ?: return@OnPreviewOutputUpdateListener

            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)
            viewFinder.surfaceTexture = it.surfaceTexture
            bufferRotation = it.rotationDegrees
            val rotation = getDisplaySurfaceRotation(viewFinder.display)
            updateTransform(viewFinder, rotation, it.textureSize, viewFinderDimensions)
            updateOverlayTransform(overlayViewRef.get(), previousAnalysisDimensions)
        }


        displayManager = viewfinder.context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        displayManager.registerDisplayListener(displayListener, null)


        viewfinder.addOnLayoutChangeListener { view, left, top, right, bottom, _, _, _, _ ->
            val viewFinder = view as TextureView
            val newViewfinderDimensions = Size(right - left, bottom - top)
            val rotation = getDisplaySurfaceRotation(viewFinder.display)
            updateTransform(viewFinder, rotation, bufferDimens, newViewfinderDimensions)
            updateOverlayTransform(overlayViewRef.get(), previousAnalysisDimensions)
        }

        viewfinder.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewDetachedFromWindow(p0: View?) {
                displayManager.registerDisplayListener(displayListener, null)
            }

            override fun onViewAttachedToWindow(p0: View?) {
                displayManager.unregisterDisplayListener(displayListener)
            }

        })


    }


    private fun updateTransform(
        viewFinder: TextureView?,
        rotation: Int?,
        newBufferDimens: Size,
        newViewFinderDimens: Size
    ) {

        val textureView = viewFinder ?: return
        if (rotation == viewFinderRotation &&
            Objects.equals(newBufferDimens, bufferDimens) &&
            Objects.equals(newViewFinderDimens, viewFinderDimensions)
        ) {
            return
        }

        if(rotation == null){
            return
        } else {
            viewFinderRotation = rotation
        }

        if(newBufferDimens.width == 0 || newBufferDimens.height == 0){
            return
        } else {
            bufferDimens = newBufferDimens
        }


        if (newViewFinderDimens.width == 0 || newViewFinderDimens.height == 0) {
            return
        } else {
            viewFinderDimensions = newViewFinderDimens
        }

        val matrix = Matrix()
        val centerX = viewFinderDimensions.width / 2f
        val centerY = viewFinderDimensions.height / 2f
        matrix.postRotate(-viewFinderRotation!!.toFloat(), centerX, centerY)

        val bufferRatio = bufferDimens.height / bufferDimens.width.toFloat()

        val scaledWidth: Int
        val scaledHeight: Int

        if(viewFinderDimensions.width > viewFinderDimensions.height){
            scaledHeight = viewFinderDimensions.width
            scaledWidth = Math.round(viewFinderDimensions.width * bufferRatio)
        } else {
            scaledHeight = viewFinderDimensions.height
            scaledWidth = Math.round((viewFinderDimensions.height * bufferRatio))
        }

        previousViewDimensions = Size(scaledWidth, scaledHeight)

        val scaleX = scaledWidth / viewFinderDimensions.width.toFloat()
        val scaleY = scaledHeight / viewFinderDimensions.height.toFloat()

        matrix.preScale(scaleX,scaleY, centerX, centerY)

        viewFinder.setTransform(matrix)
    }

    private fun updateOverlayTransform(overlayView: FacePointsView?, size: Size) {
        if (overlayView == null) {
            return
        }
        if (size == previousAnalysisDimensions) {
            return
        } else {
            previousAnalysisDimensions = size
        }
        overlayView.transformMatrix = overlayMatrix()
    }


    private fun overlayMatrix(): Matrix {
        val matrix = Matrix()

        //Scale
        val scale = previousViewDimensions.height.toFloat() / previousAnalysisDimensions.width.toFloat()
        matrix.preScale(scale, scale)


        //Set offset
        val translateX: Float
        val translateY: Float
        if (viewFinderDimensions.width > viewFinderDimensions.height) {
            translateX = (viewFinderDimensions.width - previousViewDimensions.height) / 2f
            translateY = (viewFinderDimensions.height - previousViewDimensions.width) / 2f
        } else {
            translateX = (viewFinderDimensions.width - previousViewDimensions.width) / 2f
            translateY = (viewFinderDimensions.height - previousViewDimensions.height) / 2f
        }
        matrix.postTranslate(translateX, translateY)


        val centerX = viewFinderDimensions.width / 2f
        val centerY = viewFinderDimensions.height / 2f
        matrix.postScale(-1f, 1f, centerX, centerY)

        return matrix
    }


    companion object {

        fun getDisplaySurfaceRotation(display: Display?) = when (display?.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> null
        }

        fun build(
            screenSize: Size,
            aspectRatio: Rational,
            rotation: Int,
            viewFinder: TextureView,
            overlay: FacePointsView
        ): PreviewAnalyzis {
            val previewConfig = createPreviewConfig(screenSize, aspectRatio, rotation)
            val analysisConfig = createAnalysisConfig(screenSize, aspectRatio, rotation)
            return PreviewAnalyzis(previewConfig, analysisConfig, WeakReference(viewFinder), WeakReference(overlay))
        }

        private fun createPreviewConfig(screenSize: Size, aspectRatio: Rational, rotation: Int): PreviewConfig {
            return PreviewConfig.Builder().apply {
                setLensFacing(CameraX.LensFacing.FRONT)
                setTargetResolution(screenSize)
                setTargetAspectRatio(aspectRatio)
                setTargetRotation(rotation)
            }.build()
        }

        private fun createAnalysisConfig(screenSize: Size, aspectRatio: Rational, rotation: Int): ImageAnalysisConfig {
            return ImageAnalysisConfig.Builder().apply {
                setLensFacing(CameraX.LensFacing.FRONT)
                setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
                setTargetResolution(screenSize)
                setTargetAspectRatio(aspectRatio)
                setTargetRotation(rotation)
                val handlerThread = HandlerThread("ImgProcessHandlerTherad").apply { start() }
                setCallbackHandler(Handler(handlerThread.looper))
            }.build()
        }
    }
}