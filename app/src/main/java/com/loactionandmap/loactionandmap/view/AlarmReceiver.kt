package com.loactionandmap.loactionandmap.view

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.loactionandmap.loactionandmap.R

class AlarmReceiver : BroadcastReceiver(){

    private val notificationID = 3009
    private val notificationChannelId = "notification_service_id"
    private val notificationChannelDescription = "Alarm Channel"

    private lateinit var notificationManager: NotificationManager
    private lateinit var notification: Notification

    override fun onReceive(p0: Context, p1: Intent?) {
        val intent = p1
        prepareNotification(p0)
        notificationManager.notify(1488, notification)
    }

    private fun prepareNotification(context: Context){

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val notificationChannel  = NotificationChannel(notificationChannelId, notificationChannelDescription, NotificationManager.IMPORTANCE_HIGH).apply {
                setShowBadge(true)
            }

            notificationManager.createNotificationChannel(notificationChannel)
        }

        notification = NotificationCompat.Builder(context, notificationChannelId)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentText("ContentText")
            .setContentTitle("Content Title")
            .setAutoCancel(true)
            .build()
    }

}