package com.loactionandmap.loactionandmap.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class FacePointsView(context: Context, attrs: AttributeSet) : View(context, attrs) {


    private val paint = Paint().apply {
        color = Color.CYAN
        style = Paint.Style.FILL
        strokeWidth = 4f

    }


    private val faceItemsPaint = Paint().apply {
        color = Color.CYAN
        strokeWidth = 5f
    }

    var facePoints = listOf<PointF>()
        set(value) {
            field = value
            transformPoints()
        }

    var transformMatrix = Matrix()
        set(value) {
            field = value
            transformPoints()
        }

    private var drawingPoints = listOf<PointF>()
        set(value) {
            field = value
            postInvalidate()
        }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (drawingPoints.isNotEmpty()) {
            for (i in 0 until 35) {
                paint.color = Color.rgb(180, 35, 200 - i)
                canvas?.drawCircle(drawingPoints[i].x, drawingPoints[i].y, 10f, paint)
            }
            for (i in 36..130) {
                paint.color = Color.rgb(150, 200 - i, 119)
                canvas?.drawCircle(drawingPoints[i].x, drawingPoints[i].y, 5f, paint)
            }
        }


        /*

        if(drawingPoints.isNotEmpty()) {
            for(i in 1 until drawingPoints.size) {
                paint.color = Color.rgb(200 - i, 200 - i, 200 - i)
                canvas?.drawCircle(drawingPoints[i].x, drawingPoints[i].y, 10f, paint)
            }
        }

        */
    }

    private fun transformPoints() {
        val transformInput = facePoints.flatMap { listOf(it.x, it.y) }.toFloatArray()
        val transformOutput = FloatArray(transformInput.size)
        transformMatrix.mapPoints(transformOutput, transformInput)
        drawingPoints = transformOutput.asList().chunked(2) { (x, y) ->
            PointF(x, y)
        }
    }
}
