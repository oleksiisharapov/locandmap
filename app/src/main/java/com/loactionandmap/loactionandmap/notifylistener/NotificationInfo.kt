package com.loactionandmap.loactionandmap.notifylistener

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationInfo(
    val id: Int?,
    val packageName: String?,
    val messageText: String?
): Parcelable