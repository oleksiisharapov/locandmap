package com.loactionandmap.loactionandmap.notifylistener

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.loactionandmap.loactionandmap.R
import kotlinx.android.synthetic.main.application_item_layout.view.*

class NotificationsAdapter(private val itemList: List<ApplicationItem>) :
    RecyclerView.Adapter<NotificationsAdapter.ApplicationItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApplicationItemViewHolder {
        return ApplicationItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.application_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ApplicationItemViewHolder, position: Int) {
        holder.bind(itemList[position])
    }


    inner class ApplicationItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(applicationItem: ApplicationItem) {
            view.applicationItemTitle.text = applicationItem.applicationName
            view.applicationItemNotificationsCount.text = applicationItem.applicationNotificationsCount.toString()
            try {
                view.applicationItemImageView.setImageDrawable(view.context.packageManager.getApplicationIcon(applicationItem.applicationPackage))

            } catch (e: Exception) {
                Log.d("LOCSRV", e.toString())
            }
        }

    }
}