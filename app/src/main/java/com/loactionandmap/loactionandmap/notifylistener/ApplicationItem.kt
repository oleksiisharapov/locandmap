package com.loactionandmap.loactionandmap.notifylistener

import java.util.*

data class ApplicationItem(
    var applicationName: String,
    var applicationPackage: String,
    var applicationNotificationsCount: Int,
    var lastNotificationAppeared: Date
)
