package com.loactionandmap.loactionandmap.notifylistener

import android.content.ComponentName
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager

class NotificationsListener : NotificationListenerService() {


    private var notificationListenerServiceBinder: IBinder? = null

    private var acceptNotifications = true


    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        val id = sbn?.id
        val packageName = sbn?.packageName
        val notificationText = sbn?.notification?.tickerText.toString()
        val multiMap = sbn?.notification?.extras
        val androidTitle = multiMap?.get("android.title").toString()

        val notificationObject = NotificationInfo(id, packageName, androidTitle)
        val broadcastIntent = Intent(NotificationListActivity.broadcastActionName)
        broadcastIntent.putExtra("notification", notificationObject)

        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(broadcastIntent)
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("LOCSRV", "onStartCommand")
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d("LOCSRV", "onBind")
        Log.d("LOCSRV", "onBind ${intent?.action}")
        return super.onBind(intent)
    }


    override fun onListenerDisconnected() {
        super.onListenerDisconnected()
        requestRebind(ComponentName(this, NotificationsListener::class.java))
        Log.d("LOCSRV", "Disconected")
    }

    override fun onListenerConnected() {
        super.onListenerConnected()
        requestRebind(ComponentName(this, NotificationsListener::class.java))
        Log.d("LOCSRV", "Connected")
    }

    fun acceptNotifications(accept: Boolean) {
        acceptNotifications = accept
    }

}