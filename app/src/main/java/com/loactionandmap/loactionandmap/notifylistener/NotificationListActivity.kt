package com.loactionandmap.loactionandmap.notifylistener

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.loactionandmap.loactionandmap.R
import kotlinx.android.synthetic.main.notifications_activity.*
import java.util.*

class NotificationListActivity : AppCompatActivity(), View.OnClickListener {


    companion object {
        const val broadcastActionName = "com.loactionandmap.loactionandmap.notifylistener"
        const val notificationChannelId = "NotificationServiceID"
        const val notificationChannelDescription = "NotificationChannelDescription"
    }

    private val intentFilter = IntentFilter("com.loactionandmap.loactionandmap.notifylistener")
    private val broadcastReceiver = NotificationServiceReceiver()



    val notificationManager by lazy { this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }


    val itemList = mutableListOf<ApplicationItem>()
    val notificationsListAdapter by lazy { NotificationsAdapter(itemList)}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notifications_activity)
        checkNotificationAccess()
        notifyButton.setOnClickListener { createNotification() }

        buttonStartService.setOnClickListener(this)
        buttonStopService.setOnClickListener(this)

        notificationsList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        notificationsList.adapter = notificationsListAdapter
    }


    override fun onClick(view: View?) {
        when(view?.id){
            buttonStartService.id -> startService(Intent(this, NotificationsListener::class.java))
            buttonStopService.id -> stopService(Intent(this, NotificationsListener::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }



    private fun createNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val notificationChannel = NotificationChannel(
                notificationChannelId,
                notificationChannelDescription,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val notification = NotificationCompat.Builder(this, notificationChannelId)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentText("ContentText")
            .setContentTitle("ContentTitile")
            .setTicker("TickerText")
            .build()
        notificationManager.notify(200, notification)
    }

    private fun checkNotificationAccess() {
        if (!NotificationManagerCompat.getEnabledListenerPackages(this).contains(this.packageName)) {
            startActivity(Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS))
        }
    }


    inner class NotificationServiceReceiver : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val notificationData = p1?.getParcelableExtra("notification") as NotificationInfo
            itemList.add(ApplicationItem(
                //Application name
                packageManager.getApplicationLabel(packageManager.getApplicationInfo(notificationData.packageName!!, 0)).toString(),
                notificationData.packageName,
                0,
                Date()
            ))
            notificationsListAdapter.notifyDataSetChanged()
            Toast.makeText(p0, notificationData.toString(), Toast.LENGTH_LONG).show()
        }
    }
}