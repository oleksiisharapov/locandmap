package com.loactionandmap.loactionandmap

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.loactionandmap.loactionandmap.view.AlarmReceiver
import kotlinx.android.synthetic.main.alarm_activity.*

class AlarmActivity: AppCompatActivity(){


    private val requestId = 23897
    private val alarmManager by lazy { this.getSystemService(Context.ALARM_SERVICE) as? AlarmManager }
    private lateinit var alarmIntent: PendingIntent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alarm_activity)

        alarmIntent = Intent(this, AlarmReceiver::class.java).let{
            PendingIntent.getBroadcast(this, requestId, it, 0)
        }

        buttonStartAlarm.setOnClickListener { startAlarm() }
        buttonStopAlarm.setOnClickListener { stopAlarm() }
    }

    private fun startAlarm(){
        alarmManager?.set(AlarmManager.RTC_WAKEUP, 8000, alarmIntent)
    }

    private fun stopAlarm(){
        alarmManager?.cancel(alarmIntent)
    }



}