package com.loactionandmap.loactionandmap

import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*


private const val NOTIFICATION_CHANNEL_ID = "gps_notification"
private const val NOTIFICATION__ID = 141757


class GPSLocationService : Service() {



    private val localBinder = LocalBinder()

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private val notificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }


    override fun onCreate() {
        super.onCreate()

        locationRequest = LocationRequest()
        locationRequest.interval = 4000L
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = MyLocationCallback()

    }


    override fun onBind(p0: Intent?): IBinder? {
        stopForeground(true)
        return localBinder
    }

    override fun onRebind(intent: Intent?) {
        stopForeground(true)
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {

        startForeground(NOTIFICATION__ID, createNotification())
        return true
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if(intent.getBooleanExtra(CANCEL_LOCATION, false)){
            stopTracking()
            stopSelf()
        }
        return START_NOT_STICKY

    }


    fun startTracking() {
        startService(Intent(applicationContext, GPSLocationService::class.java))
        try {
            fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallback,
                Looper.myLooper())
        } catch(e: Exception){
            Log.d("startTracking", e.message ?: "ssdd")
        }
    }

    fun stopTracking() {
        try {
            val removalTask = fusedLocationClient.removeLocationUpdates(locationCallback)
            removalTask.addOnCompleteListener {
                if (it.isSuccessful) {
                    stopSelf()
                }
            }
        } catch (e: Exception) {
            Log.d("stopTracking", e.message ?: "ssdd")
        }
    }


    private fun onNewLocation(location: Location) {
        val intent = Intent(LOCATION_BROADCAST)
        intent.putExtra("extraLoc", location)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

        notificationManager.notify(NOTIFICATION__ID, createNotification())

    }

    private fun createNotification(): Notification {

        val title = "LocationTitle"
        val notificationText = "Running..."

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID, title, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val launchActivityIntent = Intent(this, MainActivity::class.java)

        val cancelIntent = Intent(this, GPSLocationService::class.java)
        cancelIntent.putExtra(CANCEL_LOCATION,true)

        val servicePendingIntent = PendingIntent.getService(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val activityPendingIntent = PendingIntent.getActivity(this, 0, launchActivityIntent, 0)


        return NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
            .setContentTitle("Content Title")
            .setContentText("Content text")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .addAction(R.drawable.ic_launcher_background, "Activity", activityPendingIntent)
            .addAction(R.drawable.ic_chat, "Service", servicePendingIntent)
            .build()
    }




    inner class LocalBinder : Binder() {
        internal val service: GPSLocationService
            get() = this@GPSLocationService
    }

    inner class MyLocationCallback : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)

            if (locationResult?.lastLocation != null) {
                onNewLocation(locationResult.lastLocation)
            }
        }
    }

    companion object{
        internal const val PACKAGE_NAME = "com.loactionandmap.loactionandmap"
        internal const val LOCATION_BROADCAST = "LOCATION_BROADCAST"
        internal const val LOCATION_EXTRA = "$PACKAGE_NAME.extra.LOCATION"
        internal const val CANCEL_LOCATION = "CANCEL_LOCATION"
    }

}