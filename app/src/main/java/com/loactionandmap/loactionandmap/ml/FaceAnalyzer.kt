package com.loactionandmap.loactionandmap.ml

import android.graphics.PointF
import android.util.Log
import android.util.Size
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import java.util.concurrent.atomic.AtomicBoolean

class FaceAnalyzer : ImageAnalysis.Analyzer {


    private var isAnalyzing = AtomicBoolean(false)

    var pointUpdateListener: ((List<PointF>) -> Unit)? = null
    var analysisStateListener: ((Size) -> Unit)? = null


    private val faceDetector: FirebaseVisionFaceDetector by lazy {

        val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
            .build()
        FirebaseVision.getInstance().getVisionFaceDetector(options)

    }

    private val onSuccessListener = OnSuccessListener<List<FirebaseVisionFace>> { faces ->
        isAnalyzing.set(false)
        val points = mutableListOf<PointF>()
        faces.forEach {
            val contour = it.getContour(FirebaseVisionFaceContour.ALL_POINTS)
            points += contour.points.map { frbsVisionPoint -> PointF(frbsVisionPoint.x, frbsVisionPoint.y) }
        }
        pointUpdateListener?.invoke(points)
    }


    private val onFailturListener = OnFailureListener { exception ->
        isAnalyzing.set(false)
    }

    override fun analyze(image: ImageProxy?, rotationDegrees: Int) {
        if (image == null) return

        val cameraImage = image.image ?: return

        analysisStateListener?.invoke(Size(image.width, image.height))

        if (isAnalyzing.get()) {
            return
        } else {
            isAnalyzing.set(true)
        }

        val firebaseVisisonImage = FirebaseVisionImage.fromMediaImage(cameraImage, getRotationConstant(rotationDegrees))
        val result = faceDetector.detectInImage(firebaseVisisonImage)
            .addOnSuccessListener(onSuccessListener)
            .addOnFailureListener(onFailturListener)


    }

    private fun getRotationConstant(rotationDegrees: Int): Int {
        return when (rotationDegrees) {
            90 -> FirebaseVisionImageMetadata.ROTATION_90
            180 -> FirebaseVisionImageMetadata.ROTATION_180
            270 -> FirebaseVisionImageMetadata.ROTATION_270
            else -> FirebaseVisionImageMetadata.ROTATION_0
        }
    }

}