package com.loactionandmap.loactionandmap

import android.content.*
import android.graphics.Color
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ServiceConnection, OnMapReadyCallback {

    private var gpsLocationService: GPSLocationService? = null
    private var serviceBounded = false

    private lateinit var foregroundBroadcatReceiver: ForegroundBroadcastReceiver

    private lateinit var googleMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)


        foregroundBroadcatReceiver = ForegroundBroadcastReceiver()

        startServiceButton.setOnClickListener { startServiceWork() }
        stopServiceButton.setOnClickListener { stopServiceWork() }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

    }

    private fun startServiceWork() {
        gpsLocationService?.startTracking()
    }

    private fun stopServiceWork() {
        gpsLocationService?.stopTracking()
    }


    override fun onStart() {
        super.onStart()

        val serviceIntent = Intent(this, GPSLocationService::class.java)
        bindService(serviceIntent, this, Context.BIND_AUTO_CREATE)
    }


    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(foregroundBroadcatReceiver, IntentFilter(GPSLocationService.LOCATION_BROADCAST))

        customView.changeColor(Color.GRAY)
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(foregroundBroadcatReceiver)
    }

    override fun onStop() {
        if (serviceBounded) {
            unbindService(this)
            serviceBounded = false
        }

        super.onStop()
    }


    override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
        val binder = p1 as GPSLocationService.LocalBinder
        gpsLocationService = binder.service
        serviceBounded = true

    }

    override fun onServiceDisconnected(p0: ComponentName?) {
        gpsLocationService = null
        serviceBounded = false
    }


    inner class ForegroundBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val location = p1?.getParcelableExtra<Location>("extraLoc")
            if (location != null) {
                locationTextView.text = "${location.latitude} ${location.longitude}"
                googleMap.addMarker(MarkerOptions().position(LatLng(location.latitude, location.longitude)))
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude)))

            }
        }

    }


    companion object {
        private const val REQEST_LOCATION_CODE = 45487

    }


}
