package com.loactionandmap.loactionandmap

interface OnItemSwipe{

    fun swipedLeft(position: Int, layoutPosition: Int)
    fun swipedRight(position: Int, layoutPosition: Int)
    fun swipedUp(position: Int, layoutPosition: Int)

}