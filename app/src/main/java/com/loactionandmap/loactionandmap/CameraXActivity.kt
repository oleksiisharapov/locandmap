package com.loactionandmap.loactionandmap

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Rational
import android.util.Size
import android.view.Surface
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraX
import androidx.camera.core.Preview
import androidx.camera.core.PreviewConfig
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.loactionandmap.loactionandmap.view.PreviewAnalyzis
import kotlinx.android.synthetic.main.camerax_layout.*

class CameraXActivity : AppCompatActivity() {

    private val cameraRequestCode = 19001
    private var isSystemSettingsRequested = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.camerax_layout)

    }


    override fun onResume() {
        super.onResume()
        if (checkCameraPermission()) {
            initCamera()
        } else if (!isSystemSettingsRequested) {
            askCameraPermission()
        }
        //cameraView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ -> updateTransform() }
    }

    override fun onPause() {
        super.onPause()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == cameraRequestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initCamera()
            } else {
                isSystemSettingsRequested = true
                showPermissionSnackBar()
            }
        }
    }

    private fun showPermissionSnackBar() {
        if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            showSnackbar(
                "The camera access permission was denied. Open System->Settings->Application permisson",
                "Open Settings",
                Snackbar.LENGTH_INDEFINITE
            ) {
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + applicationContext.packageName)
                    ).apply {
                        addCategory(Intent.CATEGORY_DEFAULT)
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    }
                )
            }
        }
    }

    private fun checkCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askCameraPermission() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), cameraRequestCode)
    }

    private fun initCamera() {
        cameraView.post {
            CameraX.unbindAll()

            val displayMetrics = DisplayMetrics().also { cameraView.display.getRealMetrics(it) }
            val screenSize = Size(displayMetrics.widthPixels, displayMetrics.heightPixels)
            val aspectRatio = Rational(displayMetrics.widthPixels, displayMetrics.heightPixels)
            val rotation = cameraView.display.rotation

            val previewAnalyzis = PreviewAnalyzis.build(screenSize, aspectRatio, rotation, cameraView, faceView)


            CameraX.bindToLifecycle(this, previewAnalyzis.previewUseCase, previewAnalyzis.analysisUseCase)
        }
    }

    private fun updateTransform() {
        val matrix = Matrix()

        val centrX = cameraView.width / 2f
        val centrY = cameraView.height / 2f

        val rotation = when (cameraView.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }

        matrix.postRotate(-rotation.toFloat(), centrX, centrY)

        cameraView.setTransform(matrix)
    }
}

fun AppCompatActivity.showSnackbar(textMessage: String, actionText: String, duration: Int, action: () -> Unit) {
    Snackbar.make(this.window.decorView.rootView, textMessage, duration)
        .setAction(actionText) {
            action.invoke()
        }
        .show()

}


