package com.loactionandmap.loactionandmap

import android.graphics.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fastscroll.*
import kotlinx.android.synthetic.main.row_item.view.*

class FastScrollActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {


    private val listItems = listOf(
        "Atta",
        "BB",
        "FF",
        "asdgsdf",
        "Lorem ipsum dolor sit amet",
        "consectetuer adipiscing elit. Aenean ",
        "commodo ligula eget dolor. Aenean massa",
        "Cum sociis natoque penatibus et magnis",
        "dis parturient montes, nascetur ridiculus mus",
        "Donec quam felis, ultricies nec, pellentesque eu",
        "pretium quis", "sem. Nulla consequat massa ",
        "quis enim. Donec pede justo, fringilla vel",
        "aliquet nec", "vulputate eget",
        "arcu. In enim justo",
        "rhoncus ut",
        "imperdiet a",
        "venenatis vitae",
        "justo. Nullam dictum felis eu",
        "pede mollis pretium",
        "Integer tincidunt. Cras dapibus",
        "Vivamus elementum semper nisi",
        "Aenean vulputate eleifend tellus",
        "Aenean leo ligula, porttitor eu",
        "consequat vitae",
        "eleifend ac",
        "Aliquam lorem ante",
        "dapibus in",
        "Donec quam felis, ultricies nec, pellentesque eu",
        "Donec quam felis, ultricies nec, pellentesque eu",
        "commodo ligula eget dolor. Aenean massa",
        "Cum sociis natoque penatibus et magnis",
        "dis parturient montes, nascetur ridiculus mus"

    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fastscroll)
        recyclerViewFastscroll.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerViewFastscroll.addItemDecoration(TimeLineItemDecoration())
        recyclerViewFastscroll.adapter = NewAdapter(listItems)
        swiper.setOnRefreshListener(this)

    }

    override fun onRefresh() {
        (recyclerViewFastscroll.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
            listItems.size - 4,
            1200
        )
        swiper.postDelayed({ swiper.isRefreshing = false }, 3000)
    }


}

class NewAdapter(private val items: List<String>) : RecyclerView.Adapter<NewAdapter.RVViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVViewHolder {
        return RVViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RVViewHolder, position: Int) {
        holder.textView.text = items[position]
    }


    class RVViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView by lazy {
            view.row_textview
        }

    }
}

class TimeLineItemDecoration : RecyclerView.ItemDecoration() {

    val circlePaint = Paint().apply {
        color = Color.BLUE
    }

    val circleEndpoint = Paint().apply {
        color = Color.RED
        style = Paint.Style.STROKE

    }


    val linePaint = Paint().apply {
        color = Color.RED
        strokeWidth = 2f
        style = Paint.Style.STROKE
        pathEffect = DashPathEffect(floatArrayOf(5f, 10f), 0f)
    }


    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.apply {
            left = 50
            if (parent.getChildAdapterPosition(view) != parent.adapter!!.itemCount - 1) {
                bottom = 20
            }
            if (parent.getChildAdapterPosition(view) != 0) {
                top = 20
            }
        }

    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)

        val layoutManager = parent.layoutManager


        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val view = parent.getChildAt(i)

            val topDecorationWidth = layoutManager?.getTopDecorationHeight(view) ?: 0
            val bottomDecorationWidth = layoutManager?.getBottomDecorationHeight(view) ?:0
            val topLineY = view.top - topDecorationWidth
            val bottomLineY = view.top + view.height + bottomDecorationWidth

            val dotX = view.left - 20f
            val dotY = view.top + view.height / 2f
            when (parent.getChildLayoutPosition(view)) {
                0 -> {
                    c.drawLine(dotX, view.top + view.height / 2f, dotX, bottomLineY.toFloat(), linePaint)
                    c.drawCircle(dotX, dotY, 16f, circleEndpoint)
                }
                parent.adapter!!.itemCount - 1 -> {
                    c.drawLine(dotX, topLineY.toFloat(), dotX, view.top + view.height / 2f, linePaint)
                    c.drawCircle(dotX, dotY, 12f, circleEndpoint)
                }
                else -> {
                    c.drawLine(dotX, topLineY.toFloat(), dotX, bottomLineY.toFloat(), linePaint)
                    c.drawCircle(dotX, dotY, 12f, circlePaint)
                }
            }
        }
    }
}
