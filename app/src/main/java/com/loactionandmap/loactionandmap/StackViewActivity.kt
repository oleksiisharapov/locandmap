package com.loactionandmap.loactionandmap

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.stackviewactivity.*

class StackViewActivity : AppCompatActivity(), OnItemSwipe {

    private val listItems = mutableListOf(
        1, 2, 3, 4, 5, 6, 7, 8, 12
    )

    private lateinit var handler: Handler

    private val anim = object : Runnable {
        var pIndex = 1
        override fun run() {
            if (pIndex < recyclerStack.adapter!!.itemCount) {
                recyclerStack.smoothScrollToPosition(pIndex)
                pIndex++
                handler.postDelayed(this, 3000L)
            }
        }
    }

    private val swipeAnim by lazy {
        AnimationUtils.loadAnimation(this, R.anim.swipe_left)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stackviewactivity)


        val itemTouchHelper = ItemTouchHelper(SwipeHelperCallback(this))
        itemTouchHelper.attachToRecyclerView(recyclerStack)

        recyclerStack.adapter = PagerAdapter(listItems)
        recyclerStack.layoutManager = SwipeLayoutManager(this)

        handler = Handler()

        buttonYes.setOnClickListener {
            val intent = Intent(this, CameraXActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        buttonNo.setOnClickListener {
            val view = recyclerStack.getChildAt(recyclerStack.childCount - 1)
            if (view != null) {
                swipeItem(view, 0)
            }
        }

        handleIntent()
    }


    private fun handleIntent(){
        val intent = intent
        val action = intent.action!!
        if(action == Intent.ACTION_VIEW) {
            val data = intent.data
            val a = data?.getQueryParameter("a")
            val b = data?.getQueryParameters("b")
            Log.d("deepLink", "passed")
        }
    }


    private fun swipeItem(rowView: View, position: Int) {
        swipeAnim.setAnimationListener(
            object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onAnimationEnd(p0: Animation?) {

                    listItems.removeAt(0)
                    recyclerStack.adapter?.notifyDataSetChanged()

                }

                override fun onAnimationStart(p0: Animation?) {
                    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            }
        )
        rowView.startAnimation(swipeAnim)
    }


    override fun swipedLeft(position: Int, layoutPosition: Int) {
        Toast.makeText(this, "Swiped left pos=$position lp=$layoutPosition", Toast.LENGTH_SHORT).show()
        listItems.removeAt(position)
        //recyclerStack.adapter?.notifyItemRemoved(position)
        recyclerStack.adapter?.notifyDataSetChanged()
    }

    override fun swipedRight(position: Int, layoutPosition: Int) {
        Toast.makeText(this, "Swiped Right adapterpstn pos=$position lp=$layoutPosition", Toast.LENGTH_SHORT).show()
        listItems.removeAt(position)
        //recyclerStack.adapter?.notifyItemRemoved(position)
        recyclerStack.adapter?.notifyDataSetChanged()
    }

    override fun swipedUp(position: Int, layoutPosition: Int) {
        Toast.makeText(this, "Swiped UP pos=$position lp=$layoutPosition", Toast.LENGTH_SHORT).show()
        listItems.removeAt(position)
        recyclerStack.adapter?.notifyItemRemoved(position)
    }

    class PagerAdapter(private val items: List<Int>) : RecyclerView.Adapter<PagerAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false))
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.cardTitle.text = "Id = ${items[position]}"
        }


        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        }
    }




}

