package com.loactionandmap.loactionandmap

data class LocationInfo(
    var latitude: String,
    var longitude: String

)