package com.loactionandmap.loactionandmap

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class MyCustyomView(context: Context,
                    attrset: AttributeSet): View(context, attrset){

    private var color: Int = 0

    private val square = RectF()

    private lateinit var paint: Paint

    init{

        paint = Paint()

        val typedArray = context.obtainStyledAttributes(attrset, R.styleable.MyCustyomView)
        color = typedArray.getColor(R.styleable.MyCustyomView_color, Color.RED)

        paint.color = color

        typedArray.recycle()

    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        square.left = width / 2 - 50f
        square.right = width / 2 + 50f
        square.top = height /2 - 50f
        square.bottom = height /2 + 50f

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)


        canvas.drawRect(square, paint)

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val result = super.onTouchEvent(event)
        return when(event.action){
            MotionEvent.ACTION_DOWN -> true
            MotionEvent.ACTION_MOVE -> {
                val touchX = event.x
                val touchY = event.y

                if(touchX > square.left && touchX < square.right)
                    if(touchY > square.top && touchY < square.bottom){
                        square.left = touchX - 50f
                        square.right = touchX + 50f
                        square.top = touchY - 50f
                        square.bottom = touchY + 50f
                       postInvalidate()
                    }

                true
            }

            else -> result
        }
    }

    fun changeColor(color: Int){
        paint.color = color
        postInvalidate()
    }
}