package com.loactionandmap.loactionandmap

import android.content.Context
import android.graphics.PointF
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

class SwipeLayoutManager(private val context: Context) : RecyclerView.LayoutManager(), RecyclerView.SmoothScroller.ScrollVectorProvider
{


    private val maxShoweditems = 4
    private val SCALE_X = 0.1f
    private val OFFSET = 5f
    private val ROTATION = 2.5f


    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT)
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State?) {
        detachAndScrapAttachedViews(recycler)
        if (itemCount < 1) {
            return
        }

        var startPosition = Math.min(itemCount, maxShoweditems) - 1
        if (startPosition < 0) startPosition = 0

        for (itemPosition in startPosition downTo 0) {
            val view = recycler.getViewForPosition(itemPosition)
            addView(view)
            measureChildWithMargins(view, 0, 0)
            val widthSpace = width - getDecoratedMeasuredWidth(view)
            val heightSpace = height - getDecoratedMeasuredHeight(view)

            layoutDecorated(
                view, widthSpace / 2, heightSpace / 2, widthSpace / 2 + getDecoratedMeasuredWidth(view),
                heightSpace / 2 + getDecoratedMeasuredHeight(view)
            )
            if(itemPosition == 0){
                view.rotation = 0f
                view.scaleX = 1f
                view.scaleY = 1f
            }

            if (itemPosition > 0) {
                view.scaleX = 1 - SCALE_X * itemPosition
                view.scaleY = 1 - SCALE_X * itemPosition
                view.translationY = - OFFSET * itemPosition
                when (itemPosition % 2) {
                    0 -> {
                        view.translationX = OFFSET * -itemPosition
                        view.rotation = - ROTATION
                    }
                    1 -> {
                        view.translationX = OFFSET * itemPosition
                        view.rotation = ROTATION
                    }
                }

            }
        }
    }

    override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
        return null
    }

    override fun smoothScrollToPosition(recyclerView: RecyclerView?, state: RecyclerView.State?, position: Int) {
        val swipeSmoothScroller = LinearSmoothScroller(context)
        swipeSmoothScroller.targetPosition = position
        startSmoothScroll(swipeSmoothScroller)

    }
}