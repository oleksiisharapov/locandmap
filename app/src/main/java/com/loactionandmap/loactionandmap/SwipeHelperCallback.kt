package com.loactionandmap.loactionandmap

import android.graphics.Canvas
import androidx.core.view.children
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_item.view.*

class SwipeHelperCallback(private val onItemSwipe: OnItemSwipe) : ItemTouchHelper.Callback() {


    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return makeMovementFlags(0, ItemTouchHelper.START or ItemTouchHelper.END or ItemTouchHelper.UP)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        when (direction) {
            ItemTouchHelper.START -> onItemSwipe.swipedLeft(viewHolder.adapterPosition, viewHolder.layoutPosition)
            ItemTouchHelper.END -> onItemSwipe.swipedRight(viewHolder.adapterPosition, viewHolder.layoutPosition)
            ItemTouchHelper.UP -> onItemSwipe.swipedUp(viewHolder.adapterPosition, viewHolder.layoutPosition)
        }
             viewHolder.itemView.invalidate()
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        viewHolder.itemView.rotation = 45f * dX / recyclerView.measuredWidth

        var scale = Math.abs(dX / (recyclerView.width * getSwipeThreshold(viewHolder)))

        if (scale > 1){
            scale = 1f
        }

        recyclerView.getChildAt(recyclerView.childCount - 2)?.let{
            val stt =it.cardTitle.text
            it.translationY = 5f - 5 * (1 - scale)
            it.scaleX = 0.95f - 0.1f  * (1 - scale)
            it.scaleY = 0.95f - 0.1f  * (1 - scale)
            it.rotation = -2.5f + 2.5f * scale
        }

    }
}